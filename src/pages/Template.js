/**
 * Created by gabriel on 13/01/17.
 */
import React from 'react';
import Header from '../components/HeaderSite';
import Menu from '../components/menu/Menu';

const Template = () => (
  <div>
    <Header/>
    <Menu/>
    <section className='ct-main'>
      <div className='ct-content'>
        blah
      </div>
    </section>
  </div>
);

export default Template;