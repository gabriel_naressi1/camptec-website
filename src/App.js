/**
 * Created by gabriel on 13/01/17.
 */
import React from 'react';
import Template from './pages/Template';
import HomePage from './pages/home/Home';

import {Router, Route, IndexRoute, hashHistory} from 'react-router';

const App = () => (
  <Router history={hashHistory}>
    <Route component={Template} path='/'>
      <IndexRoute component={HomePage}/>
    </Route>
  </Router>
);

export default App;
