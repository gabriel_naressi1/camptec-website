/**
 * Created by gabriel on 13/01/17.
 */
import React from 'react';
import Menu from './menu/Menu';

class Header extends React.Component {

  constructor(){
    super();
    this.state = { isOpen: false }
  }

  toggleHeaderMenu() {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {

    var menu = '';
    if(this.state.isOpen) {
      menu = (
        <div className='ct-header-menu-navbar-collapse'>
          <ul>
            <li>
              <a> Home </a>
            </li>
            <li><a> Quem somos </a></li>
            <li><a> Serviços </a></li>
            <li><a> Agendamento </a></li>
            <li><a> Fale Conosco </a></li>
          </ul>
        </div>
      );
    }

    return (
      <header className='sv-header _v60 sv-bg-color--blue-800'>
        <div className='ct-logo sv-text-left'></div>
        <div className='sv-text-left'>
          <div className='ct-header-menu-area'>
            <i className='fa fa-bars' onClick={(e) => this.toggleHeaderMenu(e)}/>
          </div>
          {menu}
        </div>
      </header>
    );
  }
}

export default Header;
