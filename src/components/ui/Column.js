/**
 * Created by gabriel on 13/01/17.
 */
import React from 'react';

class Column extends React.Component {
  render() {
    return (
      <div className={this.props.className}>
        {this.props.children}
      </div>
    );
  }
}

Column.propTypes = {
  className: React.PropTypes.string,
};

Column.defaultProps = {
  className: 'sv-column',
};

export default Column;