/**
 * Created by gabriel on 13/01/17.
 */
import React from 'react';

class Row extends React.Component {
  render() {
    return (
      <div className={this.props.className}>
        {this.props.children}
      </div>
    );
  }
}

Row.propTypes = {
  className: React.PropTypes.string,
};

Row.defaultProps = {
  className: 'sv-row',
};

export default Row;