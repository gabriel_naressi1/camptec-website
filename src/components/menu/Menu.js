/**
 * Created by gabriel on 13/01/17.
 */
import React from 'react';

class Menu extends React.Component {
  render() {
    return (
      <aside className='ct-sidebar'>
        <ul>
          <li>
            <i className='fa fa-home' />
            <a>Home</a>
          </li>
          <li>
            <i className='fa fa-user-o' />
            <a>Quem somos</a>
          </li>
          <li>
            <i className='fa fa-wrench'/>
            <a>Serviços</a> </li>
          <li>
            <i className='fa fa-address-book-o'/>
            <a>Agendamento</a>
          </li>
          <li>
            <i className='fa fa-phone'/>
            <a>Fale Conosco</a>
          </li>
        </ul>
      </aside>
    )
  }
}

export default Menu;