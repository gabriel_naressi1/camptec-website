import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

let mountNode = document.getElementById('main');

ReactDOM.render(<App />, mountNode);

